Run with `gradle run` and hit the debug button to inspect the current balance,
intermediate payments and due payments. One minute in real time is one month
for the contract agent.

start. a. b :- a. c[bla(blabla)].

obligation(o6, lessee, lessor, succeeded(obligation_1_2), payRent)[deadline(monthly("10th")),active].

//sleep.

bla(a,b,c).

!start.

+start <- .print("hello world! - fact added").

+!start <- 
    ?b;
    .print("hello world! - goal").

+b <- .print("b in belief base").

+obligation_3(A,B,C,D)[active] <- .print("obligation added").

+bla(a,b,c) <- .print("ternary predicate added").

+M <- .print("random believe base update"); .print(M).

+sleep <- 
    .print("start sleeping...");
    sulfur.wait;
    .print("...waked up!").
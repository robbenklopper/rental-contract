
credit(0).


!load_modules.

!test_seconds1.
//!test_seconds2.



+!load_modules <-
    .print("loading libraries...");
    //.include("obligation_dynamics_time.asl");
    .include("obligation_dynamics_general.asl");
    .print("done.").


+!test_seconds1 <-
    //load library for handling hours, minutes, seconds instead of years, months, days
    .include("obligation_dynamics_time_insane.asl");
    /* This adds an obligation for monthly rent payments. The rent has to be payed 
    latest by the 10th of each month (deadline). Of course the obligation cannot be 
    active before the 1th of the month (date_precondition). */
    +obligation(o6, lessee, lessor, date_precondition(hour,minute,1), 
        pay(500))
        [recurring_deadline(date(year,month,10)),state(new),recurring];
    ?.time(H,M,S);
    +time_token_for_recurring_obligation(o6,timeToken(H, M, 1)).

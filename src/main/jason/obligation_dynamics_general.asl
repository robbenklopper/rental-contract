

/* obligation performed */
+obligation(ObligationName, Obligor, Beneficiary, Precond, pay(0))[state(performed)|R] <-
    .print("'+obligation': obligation was performed; ", obligation(ObligationName, Obligor, Beneficiary, Precond, pay(0))[state(performed)|R]).

/* after an instance is created from a recurring obligation, try to fulfill it 
immediately */
+obligation(ObligationName, Obligor, Beneficiary, Precond, pay(Amount))[state(active)|R] : credit(Balance) & Balance > 0 <-
    .print("'+obligation': obligation added, try to fulfill it from credit...");
    !try_to_fulfill_obligations([obligation(ObligationName, Obligor, Beneficiary, Precond, pay(Amount))[state(active)|R]]).

/* if the balance is zero, just add the obligation */
+obligation(ObligationName, Obligor, Beneficiary, Precond, pay(Amount))[state(active)|R] : credit(0) <-
    .print("'+obligation': added ", obligation(ObligationName, Obligor, Beneficiary, Precond, pay(Amount))[state(active)|R]).


/* update contracts balance and try to use the money to fulfill obligations */
+pay(Amount) : credit(Amount_old) <-
    .print("'+pay'; payment received");
    -credit(Amount_old);
    +credit(Amount_old + Amount);
    .abolish(pay(_));
    .findall(
        obligation(ObligationName, Obligor, Beneficiary, date_precondition(OY,OM,OD), Task)[state(active)],
        obligation(ObligationName, Obligor, Beneficiary, date_precondition(OY,OM,OD), Task)[state(active)], 
        PendingDues);
    .sort(PendingDues, Sorted);
    //.print(Sorted);
    !try_to_fulfill_obligations(Sorted).


/* if there is no money, nothing can be done */
+!try_to_fulfill_obligations(PendingDues) : credit(0) <-
    .print("no more credit; stop.").

/* try to fulfill as much active obligations as possible with the credit of the contract; recursion on 'Rest' */
+!try_to_fulfill_obligations([obligation(Oid, Ob, Ben, P, pay(Amount))[state(active)]|Rest]) : credit(Balance) & Balance >= Amount <-
    .print("'+!try_to_fulfill_obligations': pay full rent.");
    -obligation(Oid, Ob, Ben, P, pay(Amount))[state(active)];
    +obligation(Oid, Ob, Ben, P, pay(0))[state(performed)];
    -+credit(Balance - Amount);
    !try_to_fulfill_obligations(Rest).

/* make piecemeal payment */
+!try_to_fulfill_obligations([  obligation(Oid, Ob, Ben, P, pay(Amount))[state(active)]  |Rest]) : credit(Balance) & Balance > 0 & Balance < Amount <-
    .print("'+!try_to_fulfill_obligations': can only make piecemeal payment...");
    -obligation(Oid, Ob, Ben, P, pay(Amount))[state(active)];
    -+credit(0);
    +obligation(Oid, Ob, Ben, P, pay(Amount - Balance))[state(active)].


/* if all obligations could be payed, print remaining credits */
+!try_to_fulfill_obligations([]) <-
    ?credit(B);
    .print(credit(B)).



/* This is a library for handling recurring obligations. After a obligation 
with concrete date information was instantiated, an event has to be set up
which triggers the plan for obligation instantiation with the succeeding date
(for example the following month for a rent payment obligation)

* time token means a term containing year, month and day
* event means the addition of this token the agents belief base; this of 
course triggers plan execution

This whole mechanism is necessary to be able to execute plans in retrospect. I a
lawyer wants to review the execution of the contract, he can run the agent and 
it will stepwise process the obligations from the enactment of the contract. 
This assumes access to external data which is time-stamped. For example monthly
rent payments via a blockchain.

*/


/*
typical cycle for handling recurring obligations

*/



/* if the event of the time token for triggering instantiation of recurring 
obligations occurs, use this plan */
+time_token_for_recurring_obligation(ObligationName, timeToken(OY, OM, OD)) <-
    .print("+time_token_for_recuring_obligation: handling recurring obligation...")
    ?obligation(ObligationName, Obligor, Beneficiary, date_precondition(Y,M,D), Task )[recurring];
    +obligation(ObligationName, Obligor, Beneficiary, date_precondition(OY,OM,OD), Task)[state(active)];
    -time_token_for_recurring_obligation(ObligationName, timeToken(OY, OM, OD));
    .print("added.");
    //.print("trying to get Date for next activation of recurring obligation;");
    //get date which is used for the subsequent instance of the recurring obligation
    ?getNextTimeToken(timeToken(OY,OM,OD), date_precondition(Y,M,D), NewDate);
    //this plan has to be triggered when the obligation has to be instantiated the next time
    !create_event_for_recurring_obligation(ObligationName,NewDate);
    .print("new time token is: ", NewDate).




/*these plans can be used to get a date for recurring obligations; 
the new date is computed from a date expression (e.g. deadline) and a current
date, which serves as base for the computation.
Example: 
getNextTimeToken(timeToken(2016,2,28), date_precondition(year,month,day), NewDate) -> NewDate = date(2016,2,29)
getNextTimeToken(timeToken(2016,2,28), date_precondition(year,month,10), NewDate) -> NewDate = date(2016,3,10)
  --the second case works, but usually a date is expected that matches the date expression of date_precondition, in this case the 10th of the month

It would be possible to do is entirely with java code. But doing it in jason
might make it easier to dive in and to follow the logic. 

*/

//increasing the day has the most edge cases (february has 29 days in leap years, ...)
+?getNextTimeToken(timeToken(TY, TM, TD), date_precondition(year,month,day), NewDate) <- 
    //.print("inc by day...");
    sulfur.incDay(date(TY, TM, TD), NewDate);
    .print("?getNextTimeToken: new date: ", NewDate).
    //.print("inc by month...").

//if month is <= 11, it can savely be increased without touching the year
+?getNextTimeToken(timeToken(TY, TM, TD), 
    date_precondition(year,month,Day), NewDate) 
: .number(Day) & TM <= 11 <- 
    //.print("debug: inc by month...");
    NewDate = date(TY, TM +1, Day);
    //.print("debug: new date: ", NewDate).
    .print("?getNextTimeToken: new date: ", NewDate).

//if month is 12 and then increased, year has to be increased and month set to 1
+?getNextTimeToken(timeToken(TY, TM, TD), date_precondition(year,month,Day), NewDate) : .number(Day) & TM = 12 <- 
    //.print("inc by month...");
    NewDate = date(TY + 1, 1, Day);
    //.print("new date: ", NewDate).
    .print("?getNextTimeToken: new date: ", NewDate).
    //.print("inc by month...").

//year can always be increased without problems
+?getNextTimeToken(timeToken(TY, TM, TD), date_precondition(year,Month,Day), NewDate) : .number(Month) & .number(Day) <- 
    //.print("inc by year...");
    NewDate = date(TY + 1, Month, Day);
    //.print("new date: ", NewDate).
    .print("?getNextTimeToken: new date: ", NewDate).
    //.print("inc by month...").




/*after getting a date for the next instanciation of a recurring obligation, a
new event has to be created that triggers the plan for obligation instanciation;
depending on whether the new date lies in the past or in the future, the event
has to occur immediately or at the moment matching the real date of the token/event */
/*
+!create_time_token_for_recurring_obligation(Obligation,timeToken(TY, TM, TD)) <-
    .print("creating time token...")
    ?obligation(Obligation, Cred, Deb, Precond, Task)
    .print("obligation is: ",    obligation(Obligation, Cred, Deb, Precond, Task))
    //which plan is executed depends on which variables (TY, TM, TD) have fixed value or are 'variable' in the obligation
    ?getNextTimeToken(timeToken(TY,TM,TD), Precond, NewDate)
    //
    !create_event_for_recurring_obligation(timeToken(TY,TM,TD), NewDate)
    .print("done; new token is: ", NewDate).
*/


/*first term is internal clock of the agent; can be in the past when
reviewing a contract and starting from the first day of the contract again.
Second term is the date when the recurring obligation has to be instantiated again.*/

/*
first term is the name of the recurring obligation whose instantiation should
be initiated by the event that is created; the second argument is the date for 
the time token. This usually coincides with the date of the precondition. If 
this date is in the past, the event is issued with no delay, so the agent can 
keep catching up. If it is in the future, the event has to be delay so, that it
pops up at the date of the time token that will added to the agents believe base
by the event. 
*/

/* new time token, 'date(…,…,…)', is in the past, event has to pop up with no delay */
+!create_event_for_recurring_obligation(ObligationName, date(NY, NM, ND)) : sulfur.date(A,B,C) & a(NY,NM,ND) <= a(A,B,C)  <-
    .print("creating new event; new date from obligation is: ", date(NY, NM, ND), "; by the way: new date is in the past")
    ?.date(X,Y,Z)
    //.date(X,Y,Z)
    .concat("+time_token_for_recurring_obligation(",ObligationName, ",", timeToken(NY, NM, ND), ")", Event)
    .print(Event)
    .at("now +1 ms", Event).
    //.print("current date is: ", .date(X,Y,Z)).


/* new time token, 'date(…,…,…)' is in the future, event has to be delayed so 
that it pops up exactly at the date of 'date(…,…,…)'; therefore the delay 
between now and 'date(…,…,…)' has to be computed*/
/* TODO! insert '.at'-statement */
+!create_event_for_recurring_obligation(ObligationName, date(NY, NM, ND)) : sulfur.date(A,B,C) & a(A,B,C) < a(NY,NM,ND) <-
    .print("creating new event; new date from obligation is: ", date(NY, NM, ND), "; by the way: new date is in the future")
    ?.date(X,Y,Z)
    a(1,2,3) < a(1,2,4).
    //.date(X,Y,Z)
    //.print("current date is: ", .date(X,Y,Z)).



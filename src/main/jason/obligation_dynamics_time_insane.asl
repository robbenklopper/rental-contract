
/* ATTENTION: the content was copied from 'obligation_dynamics_general' and
was adapted for hour, minute, second instead of year, month, day  */


/* This is a library for handling recurring obligations. After a obligation 
with concrete date information was instantiated, an event has to be set up
which triggers the plan for obligation instantiation with the succeeding date
(for example the following month for a rent payment obligation)

* time token means a term containing year, month and day
* event means the addition of this token the agents belief base; this of 
course triggers plan execution

This whole mechanism is necessary to be able to execute plans in retrospect. If a
lawyer wants to review the execution of the contract, he can run the agent and 
it will stepwise process the obligations from the enactment of the contract. 
This assumes access to external data which is time-stamped. For example monthly
rent payments via a blockchain.
*/



/* if the event of the time token for triggering instantiation of recurring 
obligations occurs, use this plan */
+time_token_for_recurring_obligation(ObligationName, timeToken(OY, OM, OD)) <-
    ?obligation(ObligationName, Obligor, Beneficiary, date_precondition(Y,M,D), Task )[recurring,recurring_deadline(date(year,month,10))];
    +obligation(ObligationName, Obligor, Beneficiary, date_precondition(OY,OM,OD), Task)[state(active)];
    -time_token_for_recurring_obligation(ObligationName, timeToken(OY, OM, OD));
    //get date which is used for the subsequent instance of the recurring obligation
    ?getNextTimeToken(timeToken(OY,OM,OD), date_precondition(Y,M,D), NewDate);
    //this plan has to be triggered when the obligation has to be instantiated the next time
    !create_event_for_recurring_obligation(ObligationName,NewDate).


/*these plans can be used to get a date for recurring obligations; 
the new date is computed from a date expression (e.g. deadline) and a current
date, which serves as base for the computation.
Example: 
getNextTimeToken(timeToken(2016,2,28), date_precondition(year,month,day), NewDate) —> NewDate = date(2016,2,29)
getNextTimeToken(timeToken(2016,2,28), date_precondition(year,month,10), NewDate) —> NewDate = date(2016,3,10)
  --the second case works, but usually a date is expected that matches the date expression of date_precondition, in this case the 10th of the month instead of 28th

It would be possible to do is entirely with java code. But doing it in jason
might make it easier to dive in and to follow the logic. 

*/

//increase by one second
+?getNextTimeToken(timeToken(H,M,S), date_precondition(hour,minute,second), X) <- 
    sulfur.addSeconds(date(H,M,S), 1, NewDate);
    X = NewDate.


//if month is <= 11, it can savely be increased without touching the year
+?getNextTimeToken(timeToken(TY, TM, TD), date_precondition(hour,minute,Second), X) : .number(Second) <- 
    sulfur.addSeconds(date(TY, TM, Second), 60, NewDate);
    X = NewDate.


//year can always be increased without problems
+?getNextTimeToken(timeToken(TY, TM, TD), date_precondition(hour,Minute,Second), X) : .number(Minute) & .number(Second) <- 
    sulfur.addSeconds(date(TY, Minute, Second), 3600, NewDate);
    X = NewDate.
    



/*after getting a date for the next instantiation of a recurring obligation, a
new event has to be created that triggers the plan for obligation instantiation;
depending on whether the new date lies in the past or in the future, the event
has to occur immediately or at the moment matching the real date of the token/event */

/*
first term is the name of the recurring obligation whose instantiation should
be initiated by the event that is created; the second term is the date for 
the time token. This usually coincides with the date of the precondition. If 
this date is in the past, the event is issued with no delay, so the agent can 
keep catching up. If it is in the future, the event has to be delay so, that it
pops up at the date of the time token that will added to the agents believe base
by the event. 
*/

/* new time token, 'date(…,…,…)', is in the past, event has to pop up with no delay */
+!create_event_for_recurring_obligation(ObligationName, date(NY, NM, ND)) : .time(A,B,C) & a(NY,NM,ND) <= a(A,B,C)  <-
    .concat("+time_token_for_recurring_obligation(",ObligationName, ",", timeToken(NY, NM, ND), ")", Event)
    .at("now +10 ms", Event).


/* new time token, 'date(…,…,…)' is in the future, event has to be delayed so 
that it pops up exactly at the date of 'date(…,…,…)'; therefore the delay 
between now and 'date(…,…,…)' has to be computed*/
/* TODO! insert '.at'-statement */
+!create_event_for_recurring_obligation(ObligationName, date(NY, NM, ND)) : .time(A,B,C) & a(A,B,C) < a(NY,NM,ND) <-
    .concat("+time_token_for_recurring_obligation(",ObligationName, ",", timeToken(NY, NM, ND), ")", Event);
    sulfur.subTimeHMS(NY, NM, ND, A, B, C, DiffSeconds);
    .concat("now +", DiffSeconds, " s", Delay);
    .at(Delay, Event).



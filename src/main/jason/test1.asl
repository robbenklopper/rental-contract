

//localDate(2017, july, 12).

timetoken(o6,date(2017, june, 10)).

obligations([o6,o7, o8]).

succeeded(obligation_1_2).





//obligation(o6, lessee, lessor, succeeded(obligation_1_2), pay(rent))[deadline(monthly("10th")),state(new)].

//obligation(o7, lessee, lessor, c, pay(deposit))[state(new),bla, bla2].




o(start(year,month,1))[deadline(year,month,10)].

//decomposition_token(date(year, month, day)).

//date(year,month,1).


//initial token necessary to trigger the continous instanciation of a recurring obligation
//time_token_for_recurring_obligation(o6, timeToken(2017,12,1)).



!bla.

/*random plan to test the internal action that increments a date by one day*/
+!bla <- 
    ?getNextTimeToken(timeToken(2016,2,28), date_precondition(year,month,day), NewDate).
    //.print(NewDate).




+decomposition_token(Date) : Date = date(year, month, day) <-
    .print("decompose works!").


//next date would be incDay(date)
+date(year,month, day) <-
    .print("date with all coordinates variable occured!").

//next date would be incMonth(date)
+date(year,month, Day) : .number(Day) <-
    .print("date occured with day fixed!").

//next date would be incYear(date)
+date(year,Month,Day) : .number(Month) & .number(Day) <-
    .print("date occured with month and day fixed!").

//fixed date with no variables — no increment possible!
+date(Year, Month, Day) : .number(Year) & .number(Month) & .number(Day) <-
    .print("date occured with year, month and day fixed!").


//+spawn_time_token <- 



/*
+obligation(Obligation, Obligor, Beneficiary, Precondition, Task)[state(new)|Rest] <-
    ?Precondition;
    -obligation(Obligation, Obligor, Beneficiary, Precondition, Task);
    +obligation(Obligation, Obligor, Beneficiary, Precondition, Task)[state(active)|Rest];
    .print("obligation activated.").
*/










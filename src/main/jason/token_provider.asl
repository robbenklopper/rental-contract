// Agent token_provider in project jason_token_ring.mas2j

/* Initial beliefs and rules */
bla[source(xxx)].
token.
token_chain(["token_user1", "token_user2"]).
person("<person><alter>40</alter><name>deine_mutter</name></person>").
/* Initial goals */


/* Plans */

+token : token_chain([H|T]) <- 
	sulfur.xpathQuery("<?xml version='1.0'?><person><alter>40</alter><name>deine_mutter</name></person>","person/alter[1]",Alter);
	.print("alter: ", Alter);
	.print("alter>100? ...");
	.print("sending token to... ", H);
	.wait(1000);
	-token[source(_)];
	.send(H,tell,token);
	//-token_chain(_);
	-bla[_];
	.abolish(bla);
	.concat(T,[H],R);
	-+token_chain(R).


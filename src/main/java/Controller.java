
import jason.asSyntax.Literal;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author simon
 */
public class Controller extends jason.environment.Environment {
    
    @Override
    public void init(String args[]){
        
        WorldModel model = new WorldModel();
        WorldView view = new WorldView(model, "bla", 10);
        view.setEnv(this);
        model.setView(view);
        
        //addPercept("contract", Literal.parseLiteral("pay(500)"));
    }
    
    public void sendMoney(double amount){
        
        addPercept("contract", Literal.parseLiteral("pay(" + amount + ")"));
    }
    
}

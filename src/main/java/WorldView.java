
import jason.asSyntax.Literal;
import jason.environment.Environment;
import jason.environment.grid.GridWorldModel;
import jason.environment.grid.GridWorldView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author simon
 */
public class WorldView extends GridWorldView implements ActionListener {
    
    Environment env;
    JTextField tf1;
    JPanel panel;
    int i = 0;
    
    public WorldView(GridWorldModel model, String title, int windowSize) {
        super(model, title, windowSize);
        
        //setTitle("lessee");
        
        
        panel = new JPanel();
        
        JButton b1 = new JButton("send money");
        b1.setSize(100, 20);

        
        tf1 = new JTextField("100",10);
        tf1.setSize(1000, 20);
        add(panel);


        panel.add(b1);

        panel.add(tf1);

        b1.addActionListener(this);

        
        pack();
        
        setVisible(true);
        
        System.out.println(tf1.getText());
        
        //repaint();
    }
    
    public String getText(){
        return "bla!!!";
    }
    
    void setEnv(Environment env){
        this.env = env;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        //i++;
        //System.out.println("bla bla bla!");
        //System.out.println(tf1.getText());
        //tf1.setText("bla bla bla");
        Literal l = Literal.parseLiteral("pay(" + tf1.getText() + ")");
        //System.out.println(l.toString());
        env.clearAllPercepts();
        env.addPercept("contract", l);
        //System.out.println("2bla bla bla2!");

    }
    
    
    
}

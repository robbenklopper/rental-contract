/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sulfur;

import jason.asSemantics.*;
import jason.asSyntax.*;
import java.time.LocalTime;

/**
 *
 * @author simon
 */
public class addSeconds extends DefaultInternalAction {
    
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
        
        
        Structure s = (Structure) args[0];

        //System.out.println(args[0]);
        //System.out.println(args[1]);
        
        Integer hour = new Integer(s.getTerm(0).toString()); 
        Integer minute = new Integer(s.getTerm(1).toString());
        Integer seconds = new Integer(s.getTerm(2).toString());
        Integer secondsToAdd = new Integer(args[1].toString());
     
        
        LocalTime time = LocalTime.of(hour, minute, seconds);
        //System.out.println(time.toString());
        time = time.plusSeconds(secondsToAdd);
        //System.out.println(time.toString());
        Structure newDate = new Structure("date");

        NumberTermImpl newHour = new NumberTermImpl(time.getHour());
        NumberTermImpl newMinute = new NumberTermImpl(time.getMinute());
        NumberTermImpl newSecond = new NumberTermImpl(time.getSecond());
        
        newDate.addTerm(newHour);
        newDate.addTerm(newMinute);
        newDate.addTerm(newSecond);       

        return un.unifies(newDate, args[2]);

    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sulfur;

import jason.asSemantics.*;
import jason.asSyntax.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
/**
 *
 * @author simon
 */
public class date extends DefaultInternalAction {

    
    /*jason provides the internal action '.date(Y,M,D)', but yields a date
    where 'M' is one less than the actual month*/
    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {

        ts.getAg().getLogger().info("executing 'sulfur.date'...");

        //Date date = new Date();
        
        LocalDate date = LocalDate.now();
        
        NumberTermImpl newDay = new NumberTermImpl(date.getDayOfMonth());
        NumberTermImpl newMonth = new NumberTermImpl(date.getMonthValue());
        NumberTermImpl newYear = new NumberTermImpl(date.getYear());
        
        
        un.unifies(newYear, args[0]);
        un.unifies(newMonth, args[1]);
        un.unifies(newDay, args[2]);
        
        

        return un.unifies(newYear, args[0]);
    }
}

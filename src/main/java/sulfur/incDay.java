/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sulfur;

import jason.asSemantics.*;
import jason.asSyntax.*;
import java.time.LocalDate;
import java.time.Month;

/**
 *
 * @author simon
 */
public class incDay extends DefaultInternalAction{

    
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {

        /*this method expects as first term a structure consiting of three
        number terms*/
        Structure s = (Structure) args[0];
        ts.getAg().getLogger().info("executing 'sulfur.incDay'...");

        //System.out.println(s.toString());
        //ts.getAg().getLogger().info(s.toString() + " " + s.getTerm(0).toString());

        //year is expected to be the first term; this makes sorting dates more generic
        
        //extracting the values from the structure that was handed in a first "argument"
        Integer year = new Integer(s.getTerm(0).toString()); 
        Month month = Month.of(new Integer(s.getTerm(1).toString()));
        Integer day = new Integer(s.getTerm(2).toString());
        
        
        //use javas datetime library; it hand handle edge cases like leap years
        LocalDate date = LocalDate.of(year, month, day).plusDays(1);
        
        /* "newDate" is the predicate name that jason will see; might need
        to be changed depending on the plan where 'incDay' is used */ 
        Structure newDate = new Structure("date");
    
        NumberTermImpl newDay = new NumberTermImpl(date.getDayOfMonth());
        NumberTermImpl newMonth = new NumberTermImpl(date.getMonthValue());
        NumberTermImpl newYear = new NumberTermImpl(date.getYear());

        /* using the "builder pattern" to compose the date; 
        expected end result:  'newDate(<YEAR>, <MONTH>, <DAY>)'*/
        newDate.addTerm(newYear);
        newDate.addTerm(newMonth);
        newDate.addTerm(newDay);
        
        /* the second term of argument handed in is expected to be a free variable
        which is now unified with 'newDate' structure */
        return un.unifies(newDate, args[1]);

    }
}

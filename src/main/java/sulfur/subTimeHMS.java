/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sulfur;
import jason.asSemantics.*;
import jason.asSyntax.*;
import java.time.LocalTime;
/**
 *
 * @author simon
 */
public class subTimeHMS extends DefaultInternalAction {

    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {

        
        Integer NY = new Integer(args[0].toString());
        Integer NM = new Integer(args[1].toString());
        Integer ND = new Integer(args[2].toString());
        
        Integer A = new Integer(args[3].toString());
        Integer B = new Integer(args[4].toString());
        Integer C = new Integer(args[5].toString());
        
        LocalTime time1 = LocalTime.of(NY, NM, ND);
        //LocalTime time2 = LocalTime.of(A, B, C);
        
        time1 = time1.minusHours(A);
        time1 = time1.minusMinutes(B);
        time1 = time1.minusSeconds(C);
        
        NumberTermImpl diff = new NumberTermImpl(time1.toSecondOfDay());

        return un.unifies(diff, args[6]);

    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package sulfur;

import jason.asSemantics.*;
import jason.asSyntax.*;
import nu.xom.Builder;
import nu.xom.Document;

public class xpathQuery extends DefaultInternalAction {

    @Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
        // execute the internal action
        ts.getAg().getLogger().info("executing internal action 'sulfur.xpathQuery'");
        if (true) { // just to show how to throw another kind of exception
            //System.out.println("internal action!!!!111!!!1");
            
            String xmlDocument = args[0].toString();
            xmlDocument = xmlDocument.substring(1, xmlDocument.length() -1);
            String xpathExpression = args[1].toString();
            xpathExpression = xpathExpression.substring(1, xpathExpression.length() -1);
            System.out.println("t1: " + xmlDocument + "; t2: " + xpathExpression);
            
            Builder parser = new Builder();
            Document doc1 = parser.build(xmlDocument, null);
            String queryResult = doc1.query(xpathExpression).get(0).getValue();
            
            NumberTerm parseNumber = ASSyntax.parseNumber(queryResult);
            
            //System.out.println("query returns: " + queryResult);
            //StringTerm result = new StringTermImpl(queryResult);
            NumberTermImpl result = new NumberTermImpl(queryResult);
            //NumberTermImpl(result);
            
            //StringTerm result = StringTermImplementation
            
          
            return un.unifies(parseNumber, args[2]);
            
        }
        
        // everything ok, so returns true
        return true;
    }
}
